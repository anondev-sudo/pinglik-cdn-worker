const express = require("express");
const app = express();
const axios = require("axios");

app.set("port", 3000);
app.set("view engine", "ejs");
app.enable("trust proxy");

/* REDIRECT HTTP to HTTPS */
app.use((req, res, next) => {
  if (req.headers["x-forwarded-proto"] === "http")
    return res.redirect(301, `https://${req.headers.host}/${req.url}`);

  next();
});

const port = process.env.PORT || 3000;
const worker_name = process.env.WORKER_NAME || "Worker"

app.listen(port, () => {
  log(`[INFO] ${worker_name} webserver has started.`);
});


app.get("/u/:id", async (req, res) => {
  if (req.params.id == "upload" || req.params.id == "delfile")
    return res.status(500).json({
      error: `Use this endpoint on main domain. Just copy the URL below and use that.`,
      url: `${process.env.DOMAIN}${req.url}`,
      support: `${process.env.DOMAIN}/support`
    });
  let resp = (await axios({
    method: "GET",
    url: new URL(
      `${process.env.DOMAIN}/u/${req.params.id || "null"}`
    ).toString(),
    maxBodyLength: 5500,
    maxContentLength: 5500
  }).catch(err => {
    console.log(err);
    return res.status(500).sendFile(__dirname + "/error.jpg")
  })).data;
  // console.log(resp);
  res.writeHead(200, {
    "Content-Type": "text/html; charset=utf-8"
  });
  let reg = new RegExp(/cdn.pinglik.eu/g);
  var text = Buffer.from(resp.replace(reg, req.headers.host));

  res.end(text);
});

app.get("/u/files/:id", async (req, res) => {
  let resp = (await axios({
    method: "GET",
    url: new URL(
      `${process.env.DOMAIN}/u/files/${req.params.id || "null"}`
    ).toString(),
    responseType: "arraybuffer"
  }).catch(err => {
    console.log("Img render error: " + err);
    return res.status(500).sendFile(__dirname + "/error.jpg")
  })).data;

  var img = Buffer.from(resp, "base64");

  res.writeHead(200, {
    "Content-Type": "image/png",
    "Content-Length": img.length
  });
  res.end(img);
});
app.get("/u/e/:id", async (req, res) => {
  let resp = (await axios({
    method: "GET",
    url: new URL(
      `${process.env.DOMAIN}/u/e/${req.params.id || "null"}`
    ).toString()
  }).catch(err => {
    console.log("Oembed render error: " + err);
    return res.status(500).json({ error: "Failed to render oembed" })
  })).data;
  res.json(resp)

});

app.get("/logo.png", async (req, res) => {
  await proxyimage(`${process.env.DOMAIN}/logo.png`, res);
});
app.get("/image-not-found.png", async (req, res) => {
  await proxyimage(`${process.env.DOMAIN}/image-not-found.png`, res);
});
app.get("/arc-sw.js", async (req, res) => {
  res.status(200).sendFile(__dirname + "/arc-sw.js");
});

app.get("/api/v1/ping", async (req, res) => {
  res.status(200).json({
    success: true,
    worker: {
      domain: `${req.headers.host}`,
      name: worker_name,
      version: require("./package.json").version
    },
    main_site: process.env.host,
    support: `${process.env.DOMAIN}/support`
  });
});

app.all("*", function(req, res) {
  return res.redirect(`${process.env.DOMAIN}/?ref=${req.headers.host}`);
});



process.on("unhandledRejection", error => {
  log(`Uncaught Promise Error: \n${error.stack}`);
});
process.on("ReferenceError", error => {
  log(`ReferenceError: \n${error.stack}`);
});
process.on("uncaughtException", err => {
  const errmsg = (err ? err.stack || err : "")
    .toString()
    .replace(new RegExp(`${__dirname}/`, "g"), "./");
  log("uncaughtException\n" + errmsg);
});

function log(message) {
  console.log(message);
}

async function proxyimage(url, res) {
  let resp = (await axios({
    method: "GET",
    url: new URL(url).toString(),
    responseType: "arraybuffer"
  }).catch(err => {
    console.log("Img render error: " + err);
    return res.status(500).sendFile(__dirname + "/error.jpg")
    /* return res.status(500).json({
         error: "Image upload server error",
         support: `${process.env.DOMAIN}/support`
     });*/
  })).data;
  var img = Buffer.from(resp, "base64");
  res.writeHead(200, {
    "Content-Type": "image/png",
    "Content-Length": img.length
  });
  res.end(img);
}
const stringToRegex = str => {
  const main = str.match(/\/(.+)\/.*/)[1];
  const options = str.match(/\/.+\/(.*)/)[1];
  return new RegExp(main, options);
};

module.exports = app
